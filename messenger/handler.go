package messenger

import (
	"bitbucket.org/Skaisgiris/chat-api/config"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"net/http"
	"sync"
)

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan Message)           // broadcast channel

var messages = make([]Message, 0)
var mutex sync.Mutex

//HTTP handler for /ws endpoint
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", GetWebsocket)
	return r
}

//Handles the messenger logic for the new connection
func GetWebsocket(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	upgrader := websocket.Upgrader{}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error(err)
	}
	// Close the connection when the function returns
	defer ws.Close()

	// Register new client and send him the chat history
	mutex.Lock()
	clients[ws] = true
	mutex.Unlock()
	sendInitialMessages(ws)

	//initialize message sending logic
	sendMessageToChannel(ws)
}

// Sends messages from a particular websocket to the channel
func sendMessageToChannel(ws *websocket.Conn) {
	var msg Message
	// Read in a new message as JSON and map it to a Message object
	for {
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Info(err)
			mutex.Lock()
			delete(clients, ws)
			mutex.Unlock()
			break
		}
		broadcast <- msg
		saveMessage(msg)
	}
}

// Handles messages sent to the broadcast channel
func HandleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		// Send it out to every client that is currently connected
		mutex.Lock()
		for client := range clients {
			sendMessageToClient(msg, client)
		}
		mutex.Unlock()
	}
}

// Sends out a message to a specific client
func sendMessageToClient(msg Message, client *websocket.Conn) {
	err := client.WriteJSON(msg)
	if err != nil {
		log.Infof("error: %v", err)
		client.Close()
		mutex.Lock()
		delete(clients, client)
		mutex.Unlock()
	}
}

//Saves the message to the messages variable, checks if the amount
//of saved messages is not higher than the one described in config
func saveMessage(m Message) {
	mutex.Lock()
	if len(messages) >= config.Conf.MessageAmount {
		messages = messages[1:]
	}
	messages = append(messages, m)
	mutex.Unlock()
}

//Sends the messages stored in messages variable to a specific websocket
func sendInitialMessages(ws *websocket.Conn) {
	mutex.Lock()
	for _, m := range messages {
		err := ws.WriteJSON(m)
		if err != nil {
			log.Error(err)
		}
	}
	mutex.Unlock()
}

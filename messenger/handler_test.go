package messenger

import (
	"bitbucket.org/Skaisgiris/chat-api/config"
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"strings"
	"testing"
)

var testMessage = Message{
	Username: "Name",
	Message:  "Test message",
}

//Tests the main logic of our messenger
func Test_Messenger(t *testing.T){
	var receivedMessage Message
	//Initialize config
	config.GetConfig("../config.yaml")

	//Create test server with the InitRouter handler
	s := httptest.NewServer(InitRouter())
	defer s.Close()

	// Convert URL from http to ws
	u := "ws" + strings.TrimPrefix(s.URL, "http")

	// Connect to the test server
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}

	//Begin message handling
	go HandleMessages()

	//Send message to the server read received message and see if it's the same
	sendMessageToClient(testMessage, ws)

	//Receive the message
	err = ws.ReadJSON(&receivedMessage)
	if err != nil {
		t.Fatalf("%v", err)
	}
	//Check if it's the same message
	if receivedMessage != testMessage {
		t.Fatalf("%v", err)
	}
}

//Tests if saveMessage adds 2 more messages than the amount of save messages is set in the config
//to the messages variable and checks if the amount of messages saved is equal to the one set in config
func Test_saveMessage(t *testing.T) {
	//Initialize config
	config.GetConfig("../config.yaml")

	//Run test logic
	//initialLen := len(messages)
	for i := 0; i <= config.Conf.MessageAmount+2; i++ {
		saveMessage(testMessage)
	}
	assert.Equal(t, config.Conf.MessageAmount, len(messages), "Expected to have 1 message")
}
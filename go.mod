module bitbucket.org/Skaisgiris/chat-api

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.4.0
)

package main

import (
	"bitbucket.org/Skaisgiris/chat-api/config"
	"bitbucket.org/Skaisgiris/chat-api/messenger"
	log "github.com/sirupsen/logrus"
	"net/http"
	"github.com/go-chi/chi"
)

//Initiates configurations from the configuration file
func init(){
	config.GetConfig("config.yaml")
}

func main (){
	// Create a simple file server
	fs := http.FileServer(http.Dir("./public"))

	// Initiate router
	r := chi.NewRouter()
	r.Mount("/", fs)
	r.Mount("/ws", messenger.InitRouter())

	// Handle channel messages
	go messenger.HandleMessages()

	//Listen for HTTP requests
	log.Println("http server starting on " + config.Conf.Port)
	err := http.ListenAndServe(config.Conf.Port, r)
	if err != nil{
		log.Fatal(err)
	}
}
# chat-api #

A chat API written in Go using Websocket communication and the frontend for the chat written with Vue.

### Structure ###

This repository consist of three main directories.

* config - holds the logic of project configurations

* messenger - holds the main logic of the chat

* public - holds the frontend code

### Setup ###

Simply clone the repository and run the command `go run main.go` from the project directory.
The project will run on port `:8000` to test out the working chat simply open [http://localhost:8000/](http://localhost:8000/) .

### Configurations ###

Project configuration logic is in the config directory. It is used to make the project configurations
easier to change and update. It uses the `config.yaml`
file from the project root directory. It parses the file and adds the configurations to
the global variable `Conf` which is a `type Config struct`. Right now config only handles
the amount of message history stored and the port of the server.

To change the port of the project or the amount of message history saved you can simply
open the `config.yaml` file and update the values.

### Testing ###

Tests are written in the messenger directory `handler_test.go` file and in the config directory
`config_test.go` file. 

* `handler_test.go` tests the main logic of the chat-api, initializes configurations, 
  creates a test server and sends a test message through a websocket 
  connection. To run the test call `go test` from the messenger directory.
  
* `config_test.go` tests the logic of the projects configuration setup by passing a test 
  configuration file. To run the test call `go test` from the config directory.
  
* You can run all tests from the `chat-api` directory by calling `go test ./...`

### TODO ###

* Add database storage
* Consider adding hubs if a logic of more channels would be needed in the future
* Adding message checking logic
* Add logging logic
package config

import (
	log "github.com/sirupsen/logrus"
	"testing"
)

func Test_GetConfig(t *testing.T){
	GetConfig("test.yaml")
	if Conf.MessageAmount != 3 {
		log.Fatal("expected something else")
	}
	if Conf.Port != ":8000" {
		log.Fatal("expected something else")
	}
}

package config

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type Config struct {
	Port    string `yaml:"port"`
	MessageAmount int `yaml:"messageAmount"`
}

var Conf *Config

//Gets config from a yaml file and adds it to the global Conf variable
func GetConfig(configFile string) *Config {
	Conf = &Config{}
	if configFile != "" {
		err := Conf.getConfFromFile(configFile)
		if err != nil {
			log.Fatalf("Could not read from file: %s Error: %v\n", configFile, err)
		}
	}
	return Conf
}

func (c *Config) getConfFromFile(fileName string) error {
	pwd, _ := os.Getwd()
	yamlFile, err := ioutil.ReadFile(pwd + "/" + fileName)
	if err != nil {
		return err
	}
	return c.getConfFromString(string(yamlFile))
}

func (c *Config) getConfFromString(yamlString string) error {
	err := yaml.Unmarshal([]byte(yamlString), c)
	if err != nil {
		log.Fatalf("%s parse error %v\n", yamlString, err)
	}
	return err
}
